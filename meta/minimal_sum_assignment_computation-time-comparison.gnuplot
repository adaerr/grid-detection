set logscale
set key left
set xlabel "matrix columns"
set ylabel "computation time / s"
set title "minimal_sum_assignment.m computation time"
set yrange [5e-4:5e2]
plot "minimal_sum_assignment_commit-a3cdd03.csv" w lp t "commit-a3cdd03", "minimal_sum_assignment_commit-b80294d.csv" w lp t "commit-b80294d", 1.4e-4*x lt 7, 1.2*(x/400)**2 lt 4, 4*(x/400)**3 lt 3, 265*(x/1000)**4 lt 5
set term push
set term pdfcairo
set out "`zenity --file-selection --save --title 'Save graph as PDF...' --file-filter='Portable Document Format|*.pdf' --file-filter='All files|*' || echo /dev/null`"
rep
set out;
set term pop
