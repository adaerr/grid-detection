2D PHASE UNWRAPPING ALGORITHMS

Run QualityGuidedUnwrap2D for the phase quality guided phase unwrapping method.

Run GoldsteinUnwrap2D for Goldstein's branch cut phase unwrapping method.

(3D implementation of the quality guided method available on request)


---- edit (Adrian Daerr 2013) ----

FloodFill slightly modified to run with octave, but still seems buggy.

PhaseResidues and BranchCuts work fine with octave. To visualise
residues nicely, use:

submask = ones(size(sub));
residue_charge = PhaseResidues(sub,submask);
# normalise to [0,1]
subnorm = sub-min(min(sub));
subnorm /= max(max(subnorm));
# define color components
w = columns(sub);
h = rows(sub);
white = ones(h,w,3);
red = blue = green = zeros(h,w,3);
red(:,:,1)   = ones(h,w);
green(:,:,2) = ones(h,w);
blue(:,:,3)  = ones(h,w);
imshow(subnorm.*white);
imshow(min( max( subnorm.*white, residue_mask ), 1+residue_mask ));
