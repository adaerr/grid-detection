Note to downloaders of this directory:

This directory contains files found on the internet, and were *not* 
written by me (Adrian Daerr). In particular I do not have or claim any 
rights on these files, and can obviously not grant any. I tried to keep 
or add information on Author, Download site and/or Licence, but I 
disclaim any responsibility for your use or copying of these files.
