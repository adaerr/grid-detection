## usage: lockin_batch (name,burst,f1,f2)
## example: lockin_batch("B-50_SB2020",110,60/60,42/60);

function lockin_batch (name,burst,f1,f2)

  source(strcat(name,".meta"));
  stamps = dlmread(strcat(name,".stamps"));
  [rawfid,msg] = fopen(strcat(name,".raw"),"r");
  if ( rawfid < 0 )
    msg
    return;
  endif
  # subtract first row to have smaller values (only increment matters)
  stamps -= stamps(1,:);
  # convert times to seconds (second column has ns, third has ms)
  stamps .*= [1 1e-9 1e-3];

  imagesize = subRegionWidth * subRegionHeight;
  keepgoing = 1;
  N=0;
  Nmax=ceil(rows(stamps)/burst);
  omega1 = 2*pi*f1;
  omega2 = 2*pi*f2;

  savefields = { "phase1", "norm1", "phase2", "norm2", "offset" };
  cellfun(@(x) mkdir(dirname(name,x)), savefields, "UniformOutput", false);

  while (keepgoing && (N < Nmax))

    sa = zeros(subRegionWidth,subRegionHeight);
    sb = zeros(subRegionWidth,subRegionHeight);
    so = zeros(subRegionWidth,subRegionHeight);

    aa = 0;
    aca = 0;
    ba = 0;
    bca = 0;
    oa = 0;
    bb = 0;
    bcb = 0;
    ob = 0;
    oo = 0;

    # how many images can we expect to read for this burst ?
    maxcount = min(burst, rows(stamps) - (N*burst));

    time = stamps(N*burst+(1:maxcount),2);
    framenumber = stamps(N*burst+(1:maxcount),1);
    framenumber -= framenumber(1)-1;
    #hasframe = false(1,burst);

    fseek(rawfid, N*burst*imagesize, SEEK_SET);

    for k=1:maxcount
      [image,count] = fread(rawfid,[subRegionWidth,subRegionHeight],"uint8");
      if (count < imagesize)
	keepgoing = 0;
        break;
      endif
      ## sometimes one might want to equalize or otherwise transform the
      ## image data before calculations, e.g. take the logaritm:
      #image(image == 0) = 1;
      #image = log(image);

      if (framenumber(k) > burst)
      	break;
      endif
      #hasframe(framenumber(k)) = true;
      t = time(k);
      phase1 = exp(i*omega1*t);
      phase2 = exp(i*omega2*t);
      phase1c = conj(phase1);
      phase2c = conj(phase2);

      aa  += phase1  * phase1c;
      aca += phase1c * phase1c;
      ba  += phase2  * phase1c;
      bca += phase2c * phase1c;
      oa += phase1c;
      bb  += phase2  * phase2c;
      bcb += phase2c * phase2c;
      ob += phase2c;
      oo ++;

      sa += image*phase1c;
      sb += image*phase2c;
      so += image;

    endfor

    printf("%s\n",strcat("N=",num2str(N),", kmax=",num2str(k),", file=",num2str(ftell(rawfid)/imagesize)))
    fflush(stdout);
    if ((keepgoing == 0) && (k == 1)) # no complete images at all this burst
      disp("no complete image could be read in this burst");
      break
    endif

    M = [ aa+real(aca), imag(aca), real(ba)+real(bca), -imag(ba)+imag(bca), 2*real(oa);
      	 imag(aca), aa-real(aca), imag(ba)+imag(bca), real(ba)-real(bca), 2*imag(oa);
      	 real(ba)+real(bca), imag(ba)+imag(bca), bb+real(bcb), imag(bcb), 2*real(ob);
      	 -imag(ba)+imag(bca), real(ba)-real(bca), imag(bcb), bb-real(bcb), 2*imag(ob);
         2*real(oa), 2*imag(oa), 2*real(ob), 2*imag(ob), 2*oo ];
    # M /= burst; # have to normalise c as well, or none of the two

    c = ones(5, imagesize);
    vsa = vec(sa);
    vsb = vec(sb);
    vso = vec(so);
    c(1,:) = real(vsa);
    c(2,:) = imag(vsa);
    c(3,:) = real(vsb);
    c(4,:) = imag(vsb);
    c(5,:) = vso;
    c *= 2;

    lambda = M \ c;
    phase1 = transpose(reshape(atan2(lambda(2,:),lambda(1,:)),
                               subRegionWidth, subRegionHeight));
    norm1  = transpose(reshape(sqrt(lambda(2,:).^2 + lambda(1,:).^2),
                               subRegionWidth, subRegionHeight));
    phase2 = transpose(reshape(atan2(lambda(4,:),lambda(3,:)),
                               subRegionWidth, subRegionHeight));
    norm2  = transpose(reshape(sqrt(lambda(4,:).^2 + lambda(3,:).^2),
                               subRegionWidth, subRegionHeight));
    offset = transpose(reshape(lambda(5,:),
                               subRegionWidth, subRegionHeight));

    #figure(1)
    #imshow(offset,[]);
    #figure(2)
    #imshow(phase1,[]);

    for k=savefields,
      s=char(k);
      fid = fopen(sprintf("%s/%s_%03d.m",dirname(name,s),s,N), "w");
      fwrite(fid, eval(s)', "single", "ieee-be");
      fclose(fid);
    endfor

    N++;
  endwhile

  fclose(rawfid);
endfunction

function s = dirname(name,field)
  s = sprintf("%s_%s",name,field);
endfunction
