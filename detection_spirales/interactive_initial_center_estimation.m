# Interactively estimate the center around which the spiral pattern
# revolves
#
# Example invocation:
#
#   interactive_initial_center_estimation(240, -170,
#                                         "*phase1/phase1_*.m",
#                                         "*phase2/phase2_*.m")
#
# Select estimation points with left mouse button. Click right button
# to end the selection round, and use the middle button to reset the
# point list.

function [params1 params2 p1diff p2diff] = interactive_initial_center_estimation(N1=0, N2=0,
                      glob1name="*phase1/phase1_*.m",
                      glob2name="*phase2/phase2_*.m",
                      bgmask)

  display("Initialising..."); fflush(stdout)

  # Hardcoded parameters
  maskFile = "mask.png"; # fallback glob for valid measurement mask
  bgmaskFile = "bgmask.png"; # fallback glob for background mask

  # Input parameters can be (optionally) specified in an external file
  # defining at least the first four function parameters.
  externalParameterFile = "processing_parameters.txt";
  if (nargin < 2 || N1 == 0 || N2 == 0)
    if (length(glob(externalParameterFile)))
      disp("sourcing external parameters");
      source(externalParameterFile);
    else
      error(strcat("not enough input parameters, and no ",
                   externalParameterFile," file found"));
    endif
  endif

  # In this function we only need the first phasemap files, which we
  # load
  phase1files = sort(glob(glob1name));
  phase2files = sort(glob(glob2name));
  phase1 = load("-ascii",char(phase1files(1)));
  phase2 = load("-ascii",char(phase2files(1)));

  # Load mask identifying valid pixels
  if (( exist("mask","var") != 1 || size(mask) != size(phase1) )
      && length(glob(maskFile)))
    mask = (imread(maskFile) != 0);
  endif
  # or default to trivial mask
  if (size(mask) != size(phase1))
    mask = ones(size(phase1));
  endif

  # Load mask identifying valid background pixels
  if (( exist("bgmask","var") != 1 || size(bgmask) != size(phase1) )
      && length(glob(bgmaskFile)))
    bgmask = (imread(bgmaskFile) != 0);
  endif
  # or default to trivial mask
  if (size(bgmask) != size(phase1))
    bgmask = ones(size(phase1));
  endif

  # Now focus first on the first phasemap

  # Calculate the maximum difference to the four neighbours to
  # identify locations near phasejumps, and calculate the
  # corresponding distance map
  d = bwdist(calc_maxgrad(phase1) > pi, "chessboard");

  # And ask the user to pick estimation points
  origin1 = select_and_estimate(phase1, N1, d, bgmask);

  # Repeat on the second phasemap

  d = bwdist(calc_maxgrad(phase2) > pi, "chessboard");
  origin2 = select_and_estimate(phase2, N2, d, bgmask);

  # Print stats
  display(strcat("Estimated origin from phase 1: ",
                 num2str(mean(origin1)),
                 " +- ", num2str(sqrt(var(origin1)))));
  display(strcat("Estimated origin from phase 2: ",
                 num2str(mean(origin2)),
                 " +- ", num2str(sqrt(var(origin2)))));
  origins = [origin1; origin2];
  origin = mean(origins);
  display(["Estimated origin overall: " num2str(origin)
           " +- " num2str(sqrt(var(origins)))]);

  # [Gain from the following estimation is questionable, for now do
  # full optimisation right away]
  ## First estimate the phase offset for the pattern
  #display("Estimating phase offset".) display("For first
  #phasemap..."); params1 = fit_spirals_bg_phase_only(phase1, N1,
  #[origin 0], bgmask); display("For second phasemap..."); params2 =
  #fit_spirals_bg_phase_only(phase2, N2, [origin 0], bgmask);

  # Do an automated downhill search using the previous estimate as a
  # starting point
  display("Now searching for optimum match, please be patient.");
  fflush(stdout);

  # Multithreaded version; not stable enough for now
  #[read_fd, write_fd, err, msg] = pipe ();
  #[pid, msg] = fork(); # execute two optimisations in parallel
  #if (pid<0)
  #  error(msg)
  #elseif (pid>0)
  #  fclose(write_fd);
  #  # display("For first phasemap...");
  #  params1 = fit_spirals_bg(phase1, N1, [origin 0], bgmask);
  #  # wait child to complete
  #  params2 = fread(read_fd, [1 3], "double");
  #  fclose(read_fd);
  #  waitpid(pid);
  #else
  #  fclose(read_fd);
  #  # display("For second phasemap...");
  #  res = fit_spirals_bg(phase2, N2, [origin 0], bgmask);
  #  fwrite(write_fd, res, "double");
  #  fclose(write_fd);
  #  exit();
  #endif

  display("For first phasemap...");
  fflush(stdout);
  params1 = fit_spirals_bg(phase1, N1, [origin 0], bgmask);
  display("For second phasemap...");
  fflush(stdout);
  params2 = fit_spirals_bg(phase2, N2, params1, bgmask);

  display("Refined parameters:");
  [params1; params2]
  origin = 0.5*(params1(1:2)+params2(1:2));
  params1(1:2) = origin;
  params2(1:2) = origin;

  # Subtract background phase to obtain gradient

  slopePerPhaseshift = 1; # How much slope corresponds to a given phase shift

  ## attention: fit_spirals_calc_phase_diff result is not normalized
  ## to [-pi, pi] any more
  p1diff = fit_spirals_calc_phase_diff(phase1, N1, origin, params1(3), \
                                       mask);
  p2diff = fit_spirals_calc_phase_diff(phase2, N2, origin, params2(3), \
                                       mask);
  imagesc(p1diff);

endfunction


# Helper function to calculate maximum difference with neighbours
function maxgrad = calc_maxgrad(phasemap)

  dy=conv2([-1; 1],phasemap);
  dx=conv2([-1 1],phasemap);
  maxgrad = max(max(abs(dx(:,1:end-1)), abs(dx(:,2:end))),
                max(abs(dy(1:end-1,:)), abs(dy(2:end,:))));

endfunction


# Helper function for interactive part
function origin = select_and_estimate(phasemap, N, distancemap)

  # Prepare interactive part by showing the distance map
  imagesc(distancemap);
  display("In the figure, click on bright points far from \
perturbations (right click ends)");
  fflush(stdout);
  text(0,-30,["Click on bright points far from perturbations (right \
click to end)"],"color","r");
  count = 1;
  origin = [NaN NaN];

  # Loop to let user choose estimation points
  do
    [mx,my,mbutton]=ginput(1);
    if (mbutton == 3) # quit selection loop
      break;
    elseif (mbutton == 2) # reset selection
      origin = zeros(1,2);
      imagesc(distancemap);
      count = 1;
      continue;
    endif
    # Find point in the neighbourhood which is farthest from phase
    # jumps
    mx=round(mx);
    my=round(my);
    md=distancemap(my,mx);
    r=-md:md;
    [tmp,irow]=max(distancemap(my+r,mx+r));
    [tmp,icol]=max(tmp);
    x=mx+r(icol);
    y=my+r(irow(icol));
    md=distancemap(y,x);
    # Draw the neighbourhood we found
    hold on
    drawBox([x-md,x+md,y-md,y+md],'r');
    hold off
    oo = estimate_spiral_center(phasemap, N, [x,y], md)';
    text(x+md, y, strcat(num2str(x), ", ", num2str(y), ": ",
                         num2str(md), "\n", num2str(round(oo(1))),
                         ", ", num2str(round(oo(2)))), "color", "red")
    origin(count++,:) = oo;
  until (false)

endfunction
