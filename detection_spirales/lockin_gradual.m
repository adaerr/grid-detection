## usage: lockin (name,f1,f2)

function [accumulator1,accumulator2] = lockin_gradual (name,f1,f2)

  source(strcat(name,".meta"));
  stamps=dlmread(strcat(name,".stamps"));
  [rawfid,msg]=fopen(strcat(name,".raw"),"r");
  if ( rawfid < 0 )
    msg
    return;
  endif
  # subtract first row to have smaller values (only increment matters)
  stamps -= ones(rows(stamps),1)*stamps(1,:);

  imagesize=subRegionWidth*subRegionHeight;
  accumulator0 = zeros(subRegionWidth,subRegionHeight);
  N = 0;

  # first get mean image
  while (true)
    [image,count] = fread(rawfid,[subRegionWidth,subRegionHeight],"uint8");
    if (count < imagesize)
      break;
    endif
    accumulator0 += image;
    N++;
  endwhile
  accumulator0 /= N;
  imshow(accumulator0,[]);
  save("-ascii",strcat(name,"_mean.m"),"accumulator0");

  # then calculate Fourrier products at the two detection frequencies
  accumulator1 = zeros(subRegionWidth,subRegionHeight);
  accumulator2 = zeros(subRegionWidth,subRegionHeight);
  fseek(rawfid, 0, SEEK_SET);
  for k=1:N
    image = fread(rawfid,[subRegionWidth,subRegionHeight],"uint8");
    accumulator1 += (image - accumulator0)*exp(2*pi*i*f1*1e-9*stamps(k,2));
    accumulator2 += (image - accumulator0)*exp(2*pi*i*f2*1e-9*stamps(k,2));
    if (k > 2/min(f1,f2))# Nyquist satisfied (be it barely)
      s = strcat(name,"_f=",num2str(f1),"_n=",num2str(k));
      tmp=arg(accumulator1);
      save("-ascii",strcat(s,"_phase.m"),"tmp");
      tmp=abs(accumulator1);
      save("-ascii",strcat(s,"_norm.m"),"tmp");
      s = strcat(name,"_f=",num2str(f2),"_n=",num2str(k));
      tmp=arg(accumulator2);
      save("-ascii",strcat(s,"_phase.m"),"tmp");
      tmp=abs(accumulator2);
      save("-ascii",strcat(s,"_norm.m"),"tmp");
    endif
  endfor

  fclose(rawfid);
endfunction
