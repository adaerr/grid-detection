# Calculate the distance of the current background model to the measured
# phase maps.
# This file descends from fit_spirals_distance.m

function distance = fit_spirals_distance_both_phases(phase1, N1, phase2, N2, center, mask = 0, C = 0)

## prevent calculations involving masked pixels from contributing
if (size(mask) != size(phase1))
  mask = true(size(phase1));
endif
dxmask = mask(:,1:end-1) & mask(:,2:end);
dymask = mask(1:end-1,:) & mask(2:end,:);

phase1diff = fit_spirals_calc_phase_diff(phase1, N1, center, 0, mask, C);
phase2diff = fit_spirals_calc_phase_diff(phase2, N2, center, 0, mask, C);

## phase 1, y derivative
tmp = normalizeAngle(diff(phase1diff), 0);
d = sum(sumsq(tmp(dymask)));

## phase 2, y derivative
tmp = normalizeAngle(diff(phase2diff), 0);
d += abs(N1/N2)*sum(sumsq(tmp(dymask)));

## phase 1, x derivative
tmp = normalizeAngle(diff(phase1diff,1,2), 0);
d += sum(sumsq(tmp(dxmask)));

## phase 2, x derivative
tmp = normalizeAngle(diff(phase2diff,1,2), 0);
d += abs(N1/N2)*sum(sumsq(tmp(dxmask)));

distance = d/numel(find(mask));

endfunction
