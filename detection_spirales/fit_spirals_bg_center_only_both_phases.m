# Minimise discrepancy between measurement and fit; Ancestor is
# fit_spirals_bg, but here only the spiral center is adjusted and 
# both phase maps are fitted simultaneously.
#
# This file descends from fit_spirals_bg_center_only.m

function new_origin = fit_spirals_bg_center_only_both_phases(phase1, N1, phase2, N2, origin, mask)

maxiter = 100;
o = optimset('MaxIter', maxiter, 'TolFun', 1E-8);
x = origin;
phi = @(x) fit_spirals_distance_both_phases(phase1, N1, phase2, N2, x, mask);

[x_optim, y_min, status, iters, nevs] = powell(phi, x, o);
if (status != 1 || iters >= maxiter)
  error("Background fit bailed out after %d iterations without converging.",
        iters);
endif

###[x_optim, y_min, status, iters, nevs, lambda] = sqp(x, phi);
###if (status == 103 || iters >= maxiter)
###  error("Background fit bailed out after %d iterations without converging.",
###        iters);
###endif
###if (status == 102)
###  error("Background fit failed: BFGS update failed in sqp()");
###endif

x_optim
y_min
status
iters
nevs

new_origin = x_optim;

endfunction
