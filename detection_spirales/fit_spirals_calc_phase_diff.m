# Calculate the phase difference between the current background model and
# the measured phase map.

function phase_diff = fit_spirals_calc_phase_diff(phasemap, N, center,
                                                  offset, mask = 0, C = 0)

w = columns(phasemap);
h = rows(phasemap);
if (size(mask) != size(phasemap))
  mask = true(h,w);
endif

phase_diff = phasemap - calc_phase_matrix(w, h, N, center, offset, C);

phase_diff .*= mask;

endfunction
