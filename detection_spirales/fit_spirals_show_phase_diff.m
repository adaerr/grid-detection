# show phase difference (measurement-fit) as image

function phase_diff = fit_spirals_show_phase_diff(phasemap, N, params, mask=0)

phase_diff = fit_spirals_calc_phase_diff(phasemap, N,
                                         params(1:2), params(3), mask);

imshow(phase_diff, [-pi pi]);

endfunction
