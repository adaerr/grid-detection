# Calculate the distance of the current background model to the measured
# phase map.

function distance = fit_spirals_distance(phasemap, N, center, offset,
                                         mask = 0, C = 0)

phase_diff = fit_spirals_calc_phase_diff(phasemap, N, center, offset, mask, C);

distance = sum(sumsq(phase_diff)) / numel(find(mask));
#distance = sum(sum(imgradient(normalizeAngle(phase_diff,pi)/(2*pi)))) / numel(find(mask));
#grd=imgradient(normalizeAngle(phase_diff,pi)/(2*pi));
#grd(grd>1)=0;
#distance = sum(sum(grd)) / numel(find(mask));

endfunction
