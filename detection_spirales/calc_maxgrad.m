# Calculate maximum difference with neighbours
function maxgrad = calc_maxgrad(phasemap)

  dy=conv2([-1; 1],phasemap);
  dx=conv2([-1 1],phasemap);
  maxgrad = max(max(abs(dx(:,1:end-1)), abs(dx(:,2:end))),
                max(abs(dy(1:end-1,:)), abs(dy(2:end,:))));

endfunction
