function bginfo = calc_height_batch(glob1name="*phase1/phase1_*.m", N1,
                                    point1, width1,
                                    glob2name="*phase2/phase2_*.m", N2,
                                    point2, width2,
                                    mask,
				    params1=0, params2,
				    show=0)

# input parameters can be specified in an external file
if (nargin < 8 || length(glob1name) == 0)
  if (length(glob("processing_parameters.txt")))
    disp("sourcing external parameters");
    source("processing_parameters.txt");
  else
    error("not enough input parameters, and no processing_parameters.txt file found");
  endif
endif

hwbar = waitbar(0.0001, "initialising...");
phase1files = sort(glob(glob1name));
phase2files = sort(glob(glob2name));
maxcount = min(length(phase1files), length(phase2files));
bginfo = zeros(maxcount, 6);
h = w = 0;

for count=1:maxcount,

  progress = (count-1)/maxcount;
  waitbar(progress, hwbar, strcat("count=",num2str(count)," loading"));

  phase1 = load("-ascii",char(phase1files(count)));
  phase2 = load("-ascii",char(phase2files(count)));

  width = columns(phase1);
  height = rows(phase1);
  x = ones(height, 1) * (0 : width-1);
  y = (0 : height-1)' * ones(1, width);

  waitbar(progress, hwbar, strcat("count=",num2str(count)," background guess"));

disp(strcat("init count=",num2str(count))); tic;
  if (count == 1) # some initialisation required at the beginning

    # load mask if not already given, and if available
    if (size(mask) != size(phase1) && length(glob("mask.png")))
      mask = (imread("mask.png") != 0);
    endif
    # or default to trivial mask
    if (size(mask) != size(phase1))
      mask = ones(size(phase1));
    endif

    if (params1==0)
    # first guess at background parameters
    params1 = fit_spirals_initial_estimate(phase1, N1, point1, width1);
    params2 = fit_spirals_initial_estimate(phase2, N2, point2, width2);
    params1 = fit_spirals_bg(phase1, N1, params1, mask)
    params2 = fit_spirals_bg(phase2, N2, params2, mask)
    endif
  else # in the remaining rounds re-use former bg params, but update phase

    params1 = fit_spirals_bg_phase_only(phase1, N1, params1, mask)
    params2 = fit_spirals_bg_phase_only(phase2, N2, params2, mask)

  endif
toc

  # fit background and subtract from measurements
disp("bg 1"); tic;
  #waitbar(progress, hwbar, strcat("count=",num2str(count)," background fit 1"));
  #params1 = fit_spirals_bg(phase1, N1, params1, mask)

  waitbar(progress, hwbar, strcat("count=",num2str(count)," residual phase 1"));
  phase1 = fit_spirals_calc_phase_diff(phase1, N1, params1(1:2), params1(3),
                                       mask);
toc

  if (show)
    figure(1)
    imshow(phase1, [-pi pi]);
    title(strcat("count=",num2str(count),", phase 1"));
  endif


disp("bg 2"); tic;
  #waitbar(progress, hwbar, strcat("count=",num2str(count)," background fit 2"));
  #params2 = fit_spirals_bg(phase2, N2, params2, mask)

  waitbar(progress, hwbar, strcat("count=",num2str(count)," residual phase 2"));
  phase2 = fit_spirals_calc_phase_diff(phase2, N2, params2(1:2), params2(3),
                                       mask);
toc

  if (show)
    figure(2)
    imshow(phase2, [-pi pi]);
    title(strcat("count=",num2str(count),", phase 2"));
  endif

  # calculate gradient components along axes
  waitbar(progress, hwbar, strcat("count=",num2str(count)," cartesian grad"));

  center = 0.5*(params1(1:2)+params2(1:2));
  angle = atan2(y-center(2), x-center(1)) - pi/4;
  if (N1 < 0)
    tmp = phase1;
    phase1 = phase2;
    phase2 = tmp;
  endif

  phaseX = phase1.*cos(angle) - phase2.*sin(angle);
  phaseY = phase1.*sin(angle) + phase2.*cos(angle);

  waitbar(progress, hwbar, strcat("count=",num2str(count)," finalising"));

  if (show)
    figure(2)
    imshow(phaseX, [-pi pi]);
    title(strcat("count=",num2str(count),", phase X"));
  endif
  if (show)
    figure(3)
    imshow(phaseY, [-pi pi]);
    title(strcat("count=",num2str(count),", phase Y"));
  endif

  bginfo(count, 1:3) = params1';
  bginfo(count, 4:6) = params2';

endfor # count

endfunction
