# -- fwrite_float32_image(path, image)
#
# Write image to file as row-by-row unpadded sequence of ieee-be float32
# scalar values

function fwrite_float32_image(path, image)
  fid = fopen(path, "w");
  image = fwrite(fid, transpose(image), "float32", "ieee-be");
  fclose(fid);
endfunction
