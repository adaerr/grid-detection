# given a two frequency signal
fA = 1;
fB = 0.7;
normA = 1.3;
normB = 0.8;
phiA = 0.25*pi;
phiB = 0.65*pi;
T = 10;
N = 110;
Nsub = 100;
noise = 0.2;
strcat("building synthetic signal with fA = ", num2str(fA),
       " (norm = ", num2str(normA), ", phase/pi = ", num2str(phiA/pi),
       "), fB = ", num2str(fB),
       " (norm = ", num2str(normB), ", phase/pi = ", num2str(phiB/pi),
       ") and white noise with amplitude = ", num2str(noise))
strcat("signal period of Ts = ", num2str(10),
" time units sampled at frequency fs = ", num2str(N/T), 
", but keeping only the first Nsub = ", num2str(Nsub), " samples")
t = T*(0:(N-1))/N;
s = 1.3*cos(2*pi*fA*t + phiA) + 0.8*cos(2*pi*fB*t + phiB) + 0.2*rand(1,N);
# what are the Fourier components / scalar prod w monochromatic test functions
function p = scal(x,y) p = x*y'; endfunction
a = exp(2*pi*i*fA*t);
b = exp(2*pi*i*fB*t);
# check normalisation
strcat("check: a*a = ", num2str(scal(a,a)/N)) # should be 1
strcat("check: b*b = ", num2str(scal(b,b)/N)) # should be 1
# and check that we retrieve phase of the components of s
strcat("full sampled noisy signal has phiA/pi = ", num2str(arg(scal(s,a)/N)/pi)) # should be 0.25 without noise
strcat("full sampled noisy signal has phiB/pi = ", num2str(arg(scal(s,b)/N)/pi)) # should be 0.65 in the absence of noise
# now suppose only part of the signal could be retrieved (first Nsub samples)
nsub = 1:Nsub;
tsub = t(nsub);
# but not the complement
ncomp = (Nsub+1):N;
tcomp = t(ncomp);
# extract sub-part and complement of signal and test functions
ssub = s(nsub);
asub = a(nsub);
bsub = b(nsub);
scomp = s(ncomp);
acomp = a(ncomp);
bcomp = b(ncomp);
figure(1); plot(tsub, ssub); hold on; plot(tcomp, scomp, 'r'); hold off;
# our test (sub)functions are no longer orthonormal
strcat("components no longer orthogonal: asub*bsub = ", num2str(scal(asub,bsub)/N), " ! =  0") # non-zero
# and the extracted phases do not match the above values
phiA0 = arg(scal(ssub,asub)/N);
phiB0 = arg(scal(ssub,bsub)/N);
strcat("naive (as if orthogonal) projection yields phiA/pi = ", num2str(phiA0/pi)) # bit different from 0.25 (e.g. 0.25663)
strcat("naive (as if orthogonal) projection yields phiB/pi = ", num2str(phiB0/pi)) # bit different from 0.65 (e.g. 0.66068)
# now iterate, patching the signal in the missing region
kmax = 100;
amplif = N/(N-Nsub);
phaseA = zeros(1,kmax);
phaseB = zeros(1,kmax);
for k = 1:kmax,
  sfakecomp = cos(2*pi*fA*tcomp + phiA0) + cos(2*pi*fB*tcomp + phiB0);
  phiA1 = arg(scal(ssub,asub) + scal(sfakecomp,acomp));
  phiB1 = arg(scal(ssub,bsub) + scal(sfakecomp,bcomp));
  phaseA(k) = phiA1/pi;
  phaseB(k) = phiB1/pi;
  phiA0 = phiA1;# + amplif*(phiA1-phiA0);
  phiB0 = phiB1;# + amplif*(phiB1-phiB0);
end
strcat("patching and re-projecting (", num2str(kmax), " iterations) yields phiA/pi = ", num2str(phiA0/pi))
strcat("patching and re-projecting (", num2str(kmax), " iterations) yields phiB/pi = ", num2str(phiB0/pi))
##figure(2)
##plot(phaseA-phiA/pi, '*')
##hold on; plot(phaseB-phiB/pi, 'r*'); hold off
# example (phiA = 0.25*pi, phiB = 0.65*pi, Nsub = 800/N = 1000):
# phiA 1: 0.25024
# phiA 2: 0.24990
# phiA 3: 0.24995
# phiA 4: 0.24998
# phiA 5: 0.25000
# phiB 1: 0.65146
# phiB 2: 0.65027
# phiB 3: 0.65006
# phiB 4: 0.65002
# phiB 5: 0.65001

# following incorrect, as signal is real, see below
### ------------------------------------
### direct minimisation of the remainder
### ------------------------------------
##M = [scal(asub,asub) scal(bsub,asub); scal(asub,bsub) scal(bsub,bsub)]/N;
##c = [scal(ssub,asub); scal(ssub,bsub)]/N;
##lambda = M \ c;
##arg(lambda)/pi
##c1 = lambda(1);
##c2 = lambda(2);
### now iterate, patching the signal in the missing region
##kmax = 100;
##phaseA = zeros(1,kmax);
##phaseB = zeros(1,kmax);
##for k = 1:kmax,
##  sfakecomp = 2*abs(c1)*cos(2*pi*fA*tcomp + arg(c1)) + 2*abs(c2)*cos(2*pi*fB*tcomp + arg(c2));
##  c1 = (scal(ssub,asub) + scal(sfakecomp,acomp))/N;
##  c2 = (scal(ssub,bsub) + scal(sfakecomp,bcomp))/N;
##  phaseA(k) = arg(c1)/pi;
##  phaseB(k) = arg(c2)/pi;
##end
####figure(2)
####hold on; 
####plot(phaseA-phiA/pi,'g*')
####plot(phaseB-phiB/pi,'m*')
####hold off

# --------------------------------------------
# correct direct minimisation of the remainder
# --------------------------------------------
asubconj = conj(asub);
bsubconj = conj(bsub);
aa = scal(asub,asub)/N;
aca = scal(asubconj,asub)/N;
ba = scal(bsub,asub)/N;
bca = scal(bsubconj,asub)/N;
ab = scal(asub,bsub)/N;
acb = scal(asubconj,bsub)/N;
bb = scal(bsub,bsub)/N;
bcb = scal(bsubconj,bsub)/N;
M = [ aa + real(aca), imag(aca), real(ba) + real(bca), - imag(ba) + imag(bca);
    imag(aca), aa - real(aca), imag(ba) + imag(bca), real(ba) - real(bca);
    real(ba) + real(bca), imag(ba) + imag(bca), bb + real(bcb), imag(bcb);
    - imag(ba) + imag(bca), real(ba) - real(bca), imag(bcb), bb - real(bcb) ];
sa = scal(ssub,asub)/N;
sb = scal(ssub,bsub)/N;
c = 2*[real(sa); imag(sa); real(sb); imag(sb)];
lambda = M \ c;
strcat("correct projection: component 1 has phase = ", num2str(atan2(lambda(2),
       lambda(1))/pi), " and norm = ", num2str(sqrt(lambda(2)^2 + lambda(1)^2)))
strcat("correct projection: component 1 has phase = ", num2str(atan2(lambda(4),
       lambda(3))/pi), " and norm = ", num2str(sqrt(lambda(4)^2 + lambda(3)^2)))
