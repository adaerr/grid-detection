# given a two frequency signal
fA = 1;
fB = 0.7;
phiA = 0.25*pi;
phiB = 0.65*pi;
T = 10;
N = 110;
Nsub = 100;
t = T*(0:(N-1))/N;
s = cos(2*pi*fA*t+phiA) + cos(2*pi*fB*t+phiB) + 0.2*rand(1,N);
# what are the Fourier components / scalar prod w monochromatic test functions
function p = scal(x,y) p = x*y'; endfunction
a = exp(2*pi*i*fA*t);
b = exp(2*pi*i*fB*t);
# now suppose only part of the signal could be retrieved (first Nsub samples)
nsub = (1:N <= Nsub);
# extract sub-part and complement of signal and test functions
ssub = s(nsub);
asub = a(nsub);
bsub = b(nsub);

# --------------------------------------------
# correct direct minimisation of the remainder
# --------------------------------------------
asubconj = conj(asub);
bsubconj = conj(bsub);
aa  = scal(asub, asub)/N;
aca = scal(asubconj, asub)/N;
ba  = scal(bsub, asub)/N;
bca = scal(bsubconj, asub)/N;
bb  = scal(bsub, bsub)/N;
bcb = scal(bsubconj, bsub)/N;
M=[ aa+real(aca), imag(aca), real(ba)+real(bca), -imag(ba)+imag(bca);
    imag(aca), aa-real(aca), imag(ba)+imag(bca), real(ba)-real(bca);
    real(ba)+real(bca), imag(ba)+imag(bca), bb+real(bcb), imag(bcb);
    -imag(ba)+imag(bca), real(ba)-real(bca), imag(bcb), bb-real(bcb) ];
sa  = scal(ssub, asub)/N;
sb  = scal(ssub, bsub)/N;
c   = 2*[real(sa); imag(sa); real(sb); imag(sb)];
lambda = M \ c;
atan2(lambda(2), lambda(1))/pi
atan2(lambda(4), lambda(3))/pi
