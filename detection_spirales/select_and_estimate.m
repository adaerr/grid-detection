# Interactive selection of points and estimation of pattern center
function origin = select_and_estimate(phasemap, N, distancemap)

  # Prepare interactive part by showing the distance map
  imagesc(distancemap);
  display("In the figure, click on bright points far from \
perturbations (right click ends)");
  fflush(stdout);
  text(0,-60,["Click on bright points far from perturbations (right \
click to end)"],"color","r", "fontsize", 20);
  count = 1;
  origin = [NaN NaN];

  # Loop to let user choose estimation points
  do
    [mx,my,mbutton]=ginput(1);
    if (mbutton == 3) # quit selection loop
      break;
    elseif (mbutton == 2) # reset selection
      origin = zeros(1,2);
      imagesc(distancemap);
      count = 1;
      continue;
    endif
    # Find point in the neighbourhood which is farthest from phase
    # jumps
    mx=round(mx);
    my=round(my);
    md=distancemap(my,mx);
    r=-md:md;
    [tmp,irow]=max(distancemap(my+r,mx+r));
    [tmp,icol]=max(tmp);
    x=mx+r(icol);
    y=my+r(irow(icol));
    md=distancemap(y,x);
    # Draw the neighbourhood we found
    hold on
    drawBox([x-md,x+md,y-md,y+md],'r');
    hold off
    oo = estimate_spiral_center(phasemap, N, [x,y], md)';
    text(x+md, y, strcat(num2str(x), ", ", num2str(y), ": ",
                         num2str(md), "\n", num2str(round(oo(1))),
                         ", ", num2str(round(oo(2)))), "color", "red", "fontsize", 20);
    origin(count++,:) = oo;
  until (false)

endfunction
