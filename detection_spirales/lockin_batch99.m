## usage: lockin_batch99 (name,burst,f1,f2,f3,f4)
## example: lockin_batch99("B-50_SB2020",110,60/60,44/60,15/60,11/60);

function lockin_batch99 (name,burst,f1,f2,f3,f4)

  source(strcat(name,".meta"));
  stamps = dlmread(strcat(name,".stamps"));
  [rawfid,msg] = fopen(strcat(name,".raw"),"r");
  if ( rawfid < 0 )
    msg
    return;
  endif
  # subtract first row to have smaller values (only increment matters)
  stamps -= stamps(1,:);
  # convert times to seconds (second column has ns, third has ms)
  stamps .*= [1 1e-9 1e-3];

  imagesize = subRegionWidth * subRegionHeight;
  keepgoing = 1;
  N=0;
  Nmax=ceil(rows(stamps)/burst);
  omega1 = 2*pi*f1;
  omega2 = 2*pi*f2;
  omega3 = 2*pi*f3;
  omega4 = 2*pi*f4;

  savefields = { "phase1", "norm1", "phase2", "norm2", "offset" };
  cellfun(@(x) mkdir(dirname(name,x)), savefields, "UniformOutput", false);

  while (keepgoing && (N < Nmax))

    sa = zeros(subRegionWidth,subRegionHeight);
    sb = zeros(subRegionWidth,subRegionHeight);
    sx = zeros(subRegionWidth,subRegionHeight);
    sy = zeros(subRegionWidth,subRegionHeight);
    so = zeros(subRegionWidth,subRegionHeight);

    aa = 0;
    aca = 0;
    ba = 0;
    bca = 0;
    oa = 0;
    bb = 0;
    bcb = 0;
    ob = 0;
    oo = 0;
    xx = 0;
    xcx = 0;
    yx = 0;
    ycx = 0;
    ox = 0;
    yy = 0;
    ycy = 0;
    oy = 0;
    xa = 0;
    xca = 0;
    xb = 0;
    xcb = 0;
    ya = 0;
    yca = 0;
    yb = 0;
    ycb = 0;
    
    # how many images can we expect to read for this burst ?
    #if (N ==1)
    #maxcount = 152;
    #time = stamps(N*burst+(1:maxcount),2);
    #framenumber = stamps(N*burst+(1:maxcount),1);
    #framenumber -= framenumber(1)-1;
    #else
    #if (N > 1)
    #maxcount = min(burst, rows(stamps) - (N*burst));
    #time = stamps(N*burst+(3:maxcount+2),2);
    #framenumber = stamps(N*burst+(3:maxcount+2),1);
    #framenumber -= framenumber(1)-1 ;
    #hasframe = false(1,burst);
    #else
    maxcount = min(burst, rows(stamps) - (N*burst));
    time = stamps(N*burst+(1:maxcount),2);
    framenumber = stamps(N*burst+(1:maxcount),1);
    framenumber -= framenumber(1)-1;
    #endif
    #endif

    fseek(rawfid, N*burst*imagesize, SEEK_SET);

    for k=1:maxcount
      [image,count] = fread(rawfid,[subRegionWidth,subRegionHeight],"uint8");
      if (count < imagesize)
	keepgoing = 0;
        break;
      endif
      ## sometimes one might want to equalize or otherwise transform the
      ## image data before calculations, e.g. take the logaritm:
      #image(image == 0) = 1;
      #image = log(image);

      if (framenumber(k) > burst)
      	break;
      endif
      #hasframe(framenumber(k)) = true;
      t = time(k);
      phase1 = exp(i*omega1*t);
      phase2 = exp(i*omega2*t);
      phase1c = conj(phase1);
      phase2c = conj(phase2);

      aa  += phase1  * phase1c;
      aca += phase1c * phase1c;
      ba  += phase2  * phase1c;
      bca += phase2c * phase1c;
      oa += phase1c;
      bb  += phase2  * phase2c;
      bcb += phase2c * phase2c;
      ob += phase2c;
      oo ++;
      
      sa += image*phase1c;
      sb += image*phase2c;
      so += image;
      
      # additional modulation
      phase3 = exp(i*omega3*t);
      phase4 = exp(i*omega4*t);
      phase3c = conj(phase3);
      phase4c = conj(phase4);
      
      xx += phase3 * phase3c;
      xcx += phase3c * phase3c;
      yx += phase4 * phase3c;
      ycx += phase4c * phase3c;
      ox += phase3c;
      yy += phase4 * phase4c;
      ycy += phase4c * phase4c;
      oy += phase4c;

      xa += phase3 * phase1c;
      xca += phase3c * phase1c;
      xb += phase3 * phase2c;
      xcb += phase3c * phase2c;
      ya += phase4 * phase1c;
      yca += phase4c * phase1c;
      yb += phase4 * phase2c;
      ycb += phase4c * phase2c;
    
      sx += image*phase3c;
      sy += image*phase4c;

    endfor

    printf("%s\n",strcat("N=",num2str(N),", kmax=",num2str(k),", file=",num2str(ftell(rawfid)/imagesize)))
    fflush(stdout);
    if ((keepgoing == 0) && (k == 1)) # no complete images at all this burst
      disp("no complete image could be read in this burst");
      break
    endif


   M = [ aa+real(aca), imag(aca), real(ba)+real(bca), -imag(ba)+imag(bca), 2*real(oa);
      	imag(aca), aa-real(aca), imag(ba)+imag(bca), real(ba)-real(bca), 2*imag(oa);
        real(ba)+real(bca), imag(ba)+imag(bca), bb+real(bcb), imag(bcb), 2*real(ob);
        -imag(ba)+imag(bca), real(ba)-real(bca), imag(bcb), bb-real(bcb), 2*imag(ob);
        2*real(oa), 2*imag(oa), 2*real(ob), 2*imag(ob), 2*oo ];

    M99 = [   aa+real(aca),         imag(aca),     real(ba)+real(bca), -imag(ba)+imag(bca), real(xa)+real(xca), -imag(xa)+imag(xca), real(ya)+real(yca), -imag(ya)+imag(yca), 2*real(oa);
	        imag(aca),         aa-real(aca),   imag(ba)+imag(bca),  real(ba)-real(bca), imag(xa)+imag(xca),  real(xa)-real(xca), imag(ya)+imag(yca),  real(ya)-real(yca), 2*imag(oa);
	    real(ba)+real(bca), imag(ba)+imag(bca),   bb+real(bcb),         imag(bcb),      real(xb)+real(xcb), -imag(xb)+imag(xcb), real(yb)+real(ycb), -imag(yb)+imag(ycb), 2*real(ob);
	   -imag(ba)+imag(bca), real(ba)-real(bca),     imag(bcb),        bb-real(bcb),     imag(xb)+imag(xcb),  real(xb)-real(xcb), imag(yb)+imag(ycb),  real(yb)-real(ycb), 2*imag(ob);
	    real(xa)+real(xca), imag(xa)+imag(xca), real(xb)+real(xcb), imag(xb)+imag(xcb),     xx+real(xcx),          imag(xcx),    real(yx)+real(ycx), -imag(yx)+imag(ycx), 2*real(ox);
           -imag(xa)+imag(xca), real(xa)-real(xca),-imag(xb)+imag(xcb), real(xb)-real(xcb),      imag(xcx),          xx-real(xcx),   imag(yx)+imag(ycx),  real(yx)-real(ycx), 2*imag(ox);
            real(ya)+real(yca), imag(ya)+imag(yca), real(yb)+real(ycb), imag(yb)+imag(ycb), real(yx)+real(ycx),  imag(yx)+imag(ycx),    yy+real(ycy),          imag(ycy),     2*real(oy);
           -imag(ya)+imag(yca), real(ya)-real(yca),-imag(yb)+imag(ycb), real(yb)-real(ycb),-imag(yx)+imag(ycx),  real(yx)-real(ycx),      imag(ycy),         yy-real(ycy),    2*imag(oy);
	        2*real(oa),         2*imag(oa),        2*real(ob),          2*imag(ob),        2*real(ox),           2*imag(ox),         2*real(oy),          2*imag(oy),        2*oo ];
    
    # M /= burst; # have to normalise c as well, or none of the two

    c = ones(9, imagesize); #correct here depending on additional modulation or not
    vsa = vec(sa);
    vsb = vec(sb);
    vsx = vec(sx);
    vsy = vec(sy);
    vso = vec(so);
    c(1,:) = real(vsa);
    c(2,:) = imag(vsa);
    c(3,:) = real(vsb);
    c(4,:) = imag(vsb);
    c(5,:) = real(vsx);
    c(6,:) = imag(vsx);
    c(7,:) = real(vsy);
    c(8,:) = imag(vsy);
    c(9,:) = vso;
    c *= 2;

    lambda = M99 \ c;
    phase1 = transpose(reshape(atan2(lambda(2,:),lambda(1,:)),
                               subRegionWidth, subRegionHeight));
    norm1  = transpose(reshape(sqrt(lambda(2,:).^2 + lambda(1,:).^2 + lambda(6,:).^2 + lambda(5,:).^2 ),
                               subRegionWidth, subRegionHeight));
    phase2 = transpose(reshape(atan2(lambda(4,:),lambda(3,:)),
                               subRegionWidth, subRegionHeight));
    norm2  = transpose(reshape(sqrt(lambda(4,:).^2 + lambda(3,:).^2 + lambda(7,:).^2 + lambda(8,:).^2),
                                subRegionWidth, subRegionHeight));
    offset = transpose(reshape(lambda(9,:),
                               subRegionWidth, subRegionHeight));
    phase3 = transpose(reshape(atan2(lambda(6,:),lambda(5,:)),
                               subRegionWidth, subRegionHeight));
    phase4 = transpose(reshape(atan2(lambda(8,:),lambda(7,:)),
                               subRegionWidth, subRegionHeight));

    phase1delta = (phase1+pi-4*(phase3+pi))/(2*pi);
    phase2delta = (phase2+pi-4*(phase4+pi))/(2*pi);
    phase1 = (phase1 - 2*pi*round(phase1delta) -3*pi)/4;
    phase2 = (phase2 - 2*pi*round(phase2delta) -3*pi)/4;

    #figure(1)
    #imshow(offset,[]);
    #figure(2)
    #imshow(phase1,[]);

    for k=savefields,
      s=char(k);
      fid = fopen(sprintf("%s/%s_%03d.float32",dirname(name,s),s,N), "w");
      fwrite(fid, eval(s)', "single", "ieee-be");
      fclose(fid);
    endfor

    N++;
  endwhile

  fclose(rawfid);
endfunction

function s = dirname(name,field)
  s = sprintf("%s_%s",name,field);
endfunction
