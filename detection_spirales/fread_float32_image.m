# -- image = fread_float32_image(path, width, height)
#
# Read image stored as row-by-row unpadded sequence of ieee-be float32
# scalar values into a matrix of dimensions height x width.

function image = fread_float32_image(path, width, height)
  fid = fopen(path, "r");
  image = transpose(fread(fid, [width height], "float32", 0, "ieee-be"));
  fclose(fid);
endfunction
