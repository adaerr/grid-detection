## Integrate phase field 
## directions is a field of same size as the wrapped phase field, and 
## contains a bitmask of permissible links. [right=1,above=2,left=4,below=8]
## (i0,j0) is the starting point (whose phase is not changed); default: (1,1)
function [ unwrapped, jumps ] = flood_unwrap_phase(wrapped, directions, i0 = 1, j0 = 1)
  unwrapped = wrapped;
  jumps = [];
  iInc = [  1,  0, -1,  0 ];
  jInc = [  0, -1,  0,  1 ];
  iq = javaObject("java.util.LinkedList");
  jq = javaObject("java.util.LinkedList");
  iq.offer(i0);
  jq.offer(j0);
  while (iq.size() > 0)
    i = iq.poll();
    j = jq.poll();
    d = directions(i,j);
    if (d == 0)
      continue;
    endif
    for n=1:4
      if (bitget(d, n))       # do we have a reachable neighbour ?
        in = i + iInc(n);
        jn = j + jInc(n);
        if (directions(in, jn) > 0)
          unwrapped(in,jn) = unwrapped(i,j) + normalizeAngle(unwrapped(in,jn)
                                                             - unwrapped(i,j),
                                                             0);
          iq.offer(in);
          jq.offer(jn);
        elseif (i > 1 && i < rows(wrapped) && j > 1 && j < columns(wrapped) &&
                abs(unwrapped(in,jn) - unwrapped(i,j)) > pi)
          jumps(end+1,:) = [ i j n ];
        endif
      endif
    endfor
    directions(i,j) = 0;
  endwhile
endfunction


# directions = 15(ones(size(p1diff)));
# directions(:,1) -= 2;
# directions(:,end) -= 8;
# directions(1,:) -= 4;
# directions(end,:) -= 1;
