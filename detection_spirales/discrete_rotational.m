# Calculate the rotational of phase field on a square lattice,
# i.e. the integral of phase jumps modulo 2pi along loops linking
# four neighbouring pixels on an elementary square. Loops with
# non-zero rotational (only +1 and -1 are possible results) are
# the so-called "residues" of the field which form the end-points
# of branch-cuts in phase-unwrapping (cf. Goldstein (1988) Radio
# Science 23(4)).
#
# The result is an (N-1)x(N-1) matrix, where rot(i,j) is the rotational
# around the loop phase(i,j) -> phase(i,j+1) -> phase(i+1,j+1) -> phase(i+1,j)

function rot = discrete_rotational(phase)
  drow = normalizeAngle(phase(2:end,:)-phase(1:end-1,:), 0) / (2*pi);
  dcol = normalizeAngle(phase(:,2:end)-phase(:,1:end-1), 0) / (2*pi);
  rot = dcol(1:end-1,:) + drow(:,2:end) - dcol(2:end,:) - drow(:,1:end-1);
  rot = round(rot);
endfunction
