# Calculate phase map of given dimensions for logarithmic spiral with
# N arms and given center.
#
# Possibly orrect for a pin-cussion aberration in the experimental
# set-up. The metric curvature parameter C quantifies the scale
# increase or decrease from center to periphery. C=1 means aberration
# reaches one pixel at right and left borders. Default value is C=0,
# meaning no correction.

function phi=calc_phase_matrix(width, height, N, center, phase_offset, C = 0)

[x y] = coordinate_matrices(width, height, center, C);

phi = calc_phase(x, y, N) + phase_offset;

endfunction
