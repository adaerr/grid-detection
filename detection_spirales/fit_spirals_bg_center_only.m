# minimise discrepancy between measurement and fit; same as
# fit_spirals_bg, but allow variations of the spiral center (param(1:2)) only.

function newparams = fit_spirals_bg_center_only(phase_map, N, params, mask)

maxiter = 100;
o = optimset('MaxIter', maxiter, 'TolFun', 1E-10);
x = params(1:2);
phi = @(x) fit_spirals_distance(phase_map, N,
                                x, params(3), mask);
[x_optim, y_min, conv, iters, nevs] = powell(phi, x, o);

if (conv != 1 || iters >= maxiter)
  error("Background fit bailed out after %d iterations without converging.",
        iters);
endif

newparams = params;
newparams(1:2) = x_optim;

endfunction
