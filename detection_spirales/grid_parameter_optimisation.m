# Estimate global phase offset of the pattern in the given phasemap,
# and then refine center and phase parameters using a gradient search
# on all three parameters.

function params = grid_parameter_optimisation(N1=0, N2=0,
                                              glob1name="*phase1/phase1_*.m",
                                              glob2name="*phase2/phase2_*.m",
                                              bgmask)


# Hardcoded parameters
bgmaskFile = "bgmask.png"; # fallback glob for background mask

# Input parameters can be (optionally) specified in an external file
# defining at least the first four function parameters.
externalParameterFile = "processing_parameters.txt";
if (nargin < 2 || N1 == 0 || N2 == 0)
  if (length(glob(externalParameterFile)))
    disp("sourcing external parameters");
    source(externalParameterFile);
  else
    error(["not enough input parameters, and no " externalParameterFile " file found"]);
  endif
endif

# In this function we only need the first phasemap files, which we load
phase1files = sort(glob(glob1name));
phase2files = sort(glob(glob2name));
phase1 = load("-ascii",char(phase1files(1)));
phase2 = load("-ascii",char(phase2files(1)));

# Load mask identifying valid background pixels
if (( exist("bgmask","var") != 1 || size(bgmask) != size(phase1) )
    && length(glob(bgmaskFile)))
  bgmask = (imread(bgmaskFile) != 0);
endif
# or default to trivial mask
if (size(bgmask) != size(phase1))
  bgmask = ones(size(phase1));
endif

# First estimate the phase offset for the pattern
display("Estimating phase offset".)
display("For first phasemap...");
params1 = fit_spirals_bg_phase_only(phase1, N1, [origin 0], bgmask);
display("For second phasemap...");
params2 = fit_spirals_bg_phase_only(phase2, N2, [origin 0], bgmask);

# Do an automated downhill search using the previous estimate as a
# starting point
display("Now searching for optimum match, please be patient.");
display("For first phasemap...");
params1 = fit_spirals_bg(phase1, N1, params1, mask)
display("For second phasemap...");
params2 = fit_spirals_bg(phase2, N2, params1, mask)

params = 0.5*(params1+params2);

endfunction
