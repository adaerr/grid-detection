# Essentially a flood fill algorithm with (r0,c0) as seed pixel: all
# 4-connected neighbour pixels with id==oldId have their id replaced
# by newId, their state replaced by newState and their shift
# incremented by shiftIncrement;

function [id state shift] = phase_unwrap_merge_to_cluster(id, state, shift, r0, c0, oldId, newId, newState, shiftIncrement)

  if (oldId == newId)
    return;
  endif
  direction = [1 0; 0 1; -1 0; 0 -1];
  [rmax cmax] = size(id);

  # we have to call fill() twice with a different orientation because
  # it checks only three out of four neighbours
  fill(r0, c0, int32(1));
  fill(r0, c0, int32(2));

  # recursive flood fill
  # (r,c) is the seed
  # dir indicates direction of last step
  function fill(r, c, dir)
    # return if we are on a pixel outside the region to be filled
    if (r < 1 || r > rmax || c < 1 || c > cmax || id(r,c) != oldId)
      return;
    endif
    # "fill" pixel we stand on
    id(r,c) = newId;
    state(r,c) = newState;
    shift(r,c) += shiftIncrement;
    # now recursively fill the three unknown neighbours
    dir += 2;
    for n=1:3
      if (++dir>4)
        dir -= 4;
      endif
      fill(r+direction(dir,1), c+direction(dir,2), dir)
    endfor
  endfunction

endfunction
