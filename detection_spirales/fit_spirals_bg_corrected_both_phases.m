# Minimise discrepancy between measurement and fit; Ancestor is
# fit_spirals_bg, but here the spiral center and the aberration parameter C
# are adjusted; also both phase maps are fitted simultaneously.
#
# This file descends from fit_spirals_bg_center_only_both_phases.m

function newparams = fit_spirals_bg_corrected_both_phases(phase1, N1, phase2, N2, origin, mask, C = 0)

maxiter = 100;
o = optimset('MaxIter', maxiter, 'TolFun', 1E-8);
x = [origin C];
phi = @(x) fit_spirals_distance_both_phases(phase1, N1, phase2, N2, x(1:2), mask, x(3));

[x_optim, y_min, status, iters, nevs] = powell(phi, x, o);
if (status != 1 || iters >= maxiter)
  error("Background fit bailed out after %d iterations without converging.",
        iters);
endif

x_optim
y_min
status
iters
nevs

newparams = x_optim;

endfunction
