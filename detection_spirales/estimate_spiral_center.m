# Guess the position of the center of logarithmic spirals, by analysing
# the gradient in a square around a given point on a phase map.
#
# Parameters:
# phasemap ... phase measurement
#
# point, hwidth ... center and half-width of a square on the phasemap
# that is reasonably free from large phase jumps. Typically <point> will
# point to the middle of an arbitrary phase stripe (in pixel
# coordinates, i.e. the top left element has coords 0,0), and <hwidth>
# is the L_inf distance to the next phasejump (stripe border) in
# pixels
#
# N ... the number of spiral arms; positive values represent counterclockwise
# spirals, negative values stand for clockwise spirals
#
# Returns:
# center ... the estimated center of the spiral pattern, relative to the
# same origin as <point>. 2x1 column vector.

function origin = estimate_spiral_center(phasemap, N, point, hwidth)

if (hwidth<1)
  error("estimate_spiral_center: hwidth must be at least 1 !\n");
endif
if (rows(point) == 1)
  point = point';
endif
hwidth = round(hwidth);
offset = (-hwidth):hwidth;
# calculate normalisation constant
weight = sumsq(offset) * (2*hwidth+1);
# extract region of interest
point_row = point(2)+1;
point_column = point(1)+1;
region = phasemap(point_row + offset, point_column + offset);

# some broadcasting happening in the following;
# it's intentional, so turn off the corresponding warning
save_state = warning("query", "Octave:broadcast").state;
warning("off", "Octave:broadcast");

# gradient from weighted integral
gradient = [sum(sum(region.*offset)); sum(sum(region.*offset'))] / weight;

# restore broadcasting warning to previous state
warning(save_state, "Octave:broadcast");

# use gradient direction an norm to find origin
r = abs(N)*sqrt(2)/norm(gradient);
alpha = pi/4*(2-sign(N));
R = [cos(alpha) sin(alpha); -sin(alpha) cos(alpha)];

origin = point + r*R*gradient/norm(gradient);

endfunction
