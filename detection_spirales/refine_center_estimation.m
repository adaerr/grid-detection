# Estimate global phase offset of the pattern in the given phasemap,
# and then refine center and phase parameters using a gradient search
# on all three parameters.

function grid_parameter_estimation(N1=0, N2=0,
                      glob1name="*phase1/phase1_*.m",
                      glob2name="*phase2/phase2_*.m",
                      bgmask)

# Do an automated downhill search using the previous estimate as a
# starting point
display("Now searching for optimum match, please be patient.");
display("For first phasemap...");
params1 = fit_spirals_bg(phase1, N1, [origin 0], mask)
display("For second phasemap...");
params2 = fit_spirals_bg(phase2, N2, [origin 0], mask)

params = 0.5*(params1+params2);
