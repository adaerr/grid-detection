# given a two frequency signal
fA = 1;
fB = 0.7;
normA = 1.3;
normB = 0.8;
phiA = 0.25*pi;
phiB = 0.65*pi;
offset = 0.6;
T = 10;
N = 110;
Nsub = 90;
noise = 0.4;
strcat("building synthetic signal with fA = ", num2str(fA),
       " (norm = ", num2str(normA), ", phase/pi = ", num2str(phiA/pi),
       "), fB = ", num2str(fB),
       " (norm = ", num2str(normB), ", phase/pi = ", num2str(phiB/pi),
       "), offset = ", num2str(offset),
       ", and white noise with amplitude = ", num2str(noise))
strcat("signal period of Ts = ", num2str(10),
       " time units sampled at frequency fs = ", num2str(N/T), 
       ", but keeping only the first Nsub = ", num2str(Nsub), " samples")
t = T*(0:(N-1))/N;
s = offset + 1.3*cos(2*pi*fA*t + phiA) + 0.8*cos(2*pi*fB*t + phiB) - 0.5*noise + noise*rand(1,N);
# what are the Fourier components / scalar prod w monochromatic test functions
function p = scal(x,y) p = x*y'; endfunction
a = exp(2*pi*i*fA*t);
b = exp(2*pi*i*fB*t);
one = ones(1,N);
# check normalisation
##strcat("check: a*a = ", num2str(scal(a,a)/N)) # should be 1
##strcat("check: b*b = ", num2str(scal(b,b)/N)) # should be 1
# and check that we retrieve phase of the components of s
strcat("full sampled noisy signal has offset = ", num2str(scal(s,one)/N)) # see top for value in the absence of noise
strcat("full sampled noisy signal has phiA/pi = ", num2str(arg(scal(s,a)/N)/pi)) # should be 0.25 without noise
strcat("full sampled noisy signal has phiB/pi = ", num2str(arg(scal(s,b)/N)/pi)) # should be 0.65 in the absence of noise
# now suppose only part of the signal could be retrieved (first Nsub samples)
nsub = (1:N <= Nsub);
tsub = t(nsub);
# extract sub-part of signal and test functions
ssub = s(nsub);
asub = a(nsub);
bsub = b(nsub);
osub = one(nsub);
figure(1); plot(tsub, ssub); hold on; plot(t(!nsub), s(!nsub), 'r'); hold off;

# our test (sub)functions are no longer orthonormal
strcat("components no longer orthogonal: asub*bsub = ", num2str(scal(asub,bsub)/N), " ! =  0") # non-zero
# and the extracted phases do not match the above values
phiA0 = arg(scal(ssub,asub)/N);
phiB0 = arg(scal(ssub,bsub)/N);
strcat("naive (as if orthogonal) projection yields offset = ", num2str(scal(ssub,osub))) # bit different from value above
strcat("naive (as if orthogonal) projection yields phiA/pi = ", num2str(phiA0/pi)) # bit different from 0.25 (e.g. 0.25663)
strcat("naive (as if orthogonal) projection yields phiB/pi = ", num2str(phiB0/pi)) # bit different from 0.65 (e.g. 0.66068)

# --------------------------------------------
# correct direct minimisation of the remainder
# --------------------------------------------
asubconj = conj(asub);
bsubconj = conj(bsub);
aa = scal(asub,asub)/N;
aca = scal(asubconj,asub)/N;
ba = scal(bsub,asub)/N;
bca = scal(bsubconj,asub)/N;
oa = scal(osub,asub)/N;
bb = scal(bsub,bsub)/N;
bcb = scal(bsubconj,bsub)/N;
ob = scal(osub,bsub)/N;
oo = scal(osub,osub)/N;
M = [ aa + real(aca), imag(aca), real(ba) + real(bca), - imag(ba) + imag(bca), 2*real(oa);
    imag(aca), aa - real(aca), imag(ba) + imag(bca), real(ba) - real(bca), 2*imag(oa);
    real(ba) + real(bca), imag(ba) + imag(bca), bb + real(bcb), imag(bcb), 2*real(ob);
    - imag(ba) + imag(bca), real(ba) - real(bca), imag(bcb), bb - real(bcb), 2*imag(ob);
    2*real(oa), 2*imag(oa), 2*real(ob), 2*imag(ob), 2*oo ];
sa = scal(ssub,asub)/N;
sb = scal(ssub,bsub)/N;
so = scal(ssub,osub)/N;
c = 2*[real(sa); imag(sa); real(sb); imag(sb); so];
lambda = M \ c;
strcat("projection: signal has offset = ", num2str(lambda(5)))
strcat("projection: component 1 has phase = ", num2str(atan2(lambda(2),
       lambda(1))/pi), " and norm = ", num2str(sqrt(lambda(2)^2 + lambda(1)^2)))
strcat("projection: component 2 has phase = ", num2str(atan2(lambda(4),
       lambda(3))/pi), " and norm = ", num2str(sqrt(lambda(4)^2 + lambda(3)^2)))
