function [ optimumRows ] = minimal_sum_assignment( cost )
  %% MinimalSumAssignment find assignment with minimal cost
  %%
  %% Given a square matrix cost(i,j), solve the assignment problem with
  %% minimal total cost, i.e. find pairs (i_j,j) assigning a row i_j to
  %% every column j so that sum_j(cost(i_j,j)) is minimal and (j != k)
  %% implies (i_j != i_k).
  %%
  %% Returns a vector containing optimum row indices i_opt=optimumRow(j)
  %% for columns j.
  %%
  %% Implemented using the Hungarian algorithm as described on
  %% http://de.wikipedia.org/wiki/Ungarische_Methode
  %% (Read on 2014-12-15)
  %%
  %% Copyright 2014 Adrian Daerr
  %% This work is licensed under a Creative Commons
  %% Attribution-ShareAlike 4.0 International Licence 
  %% (http://creativecommons.org/)
  %% In particular no warranties are given.

## Check input argument
[N nc] = size(cost);
if (N != nc)
  error('parameter must be a square matrix');
endif
# Some expressions in the following rely on broadcasting;
# it's intentional, so turn off the corresponding warning
save_state = warning("query", "Octave:broadcast").state;
warning("off", "Octave:broadcast");

## Initialise auxiliary vector indicating the row index of a mark
## in column j (or 0 if there is none in that column)
s = zeros(N,1);

## Step 1: Subtract the minimum in each row and each column
v = min(cost);                  # column minima
u = min(cost - v, [], 2);       # row minima
C = cost - u - v;               # expression simple thanks to automatic
                                # expansion (broadcasting)

## Step 2: check if we can mark zeros such that every row and
## every column contains only one mark

## first mark all zeros unique in their respective column/row
colsToCheck = ones(1,N,"logical");
rowsToCheck = ones(1,N,"logical");
while(true)
  colsToReCheck = zeros(1,N,"logical");
  # go through all columns not yet marked or found to be free of zeros
  for j=find(colsToCheck)
    i = find(C(rowsToCheck,j) == 0);
    if (length(i) == 1)        # single zero => mark
      i = find(rowsToCheck)(i); # find index in full matrix
      s(j) = i;
      rowsToCheck(i) = false;
    elseif (length(i) > 1)     # multiple zeros => check again later
      colsToReCheck(j) = true;
    endif
  endfor
  rowsToReCheck = zeros(1,N,"logical");
  for i=find(rowsToCheck)
    j = find(C(i,colsToReCheck) == 0);
    if (length(j) == 1)        # single zero => mark
      j = find(colsToReCheck)(j);
      s(j) = i;
      colsToReCheck(j) = false;
    elseif (length(j) > 1)     # multiple zeros => check again later
      rowsToReCheck(i) = true;
    endif
  endfor
  # any progress ?
  if ((length(colsToReCheck) == 0 && length(rowsToReCheck) == 0) ||
      (colsToCheck == colsToReCheck && rowsToCheck == rowsToReCheck))
    break;
  endif
  colsToCheck = colsToReCheck;
  rowsToCheck = rowsToReCheck;
endwhile

## by now only rows and columns with multiple zeros are left, if any;
## for now mark arbitrarily
for j=find(colsToCheck)
  i = find(C(rowsToCheck,j) == 0);
  i = find(rowsToCheck)(i(1));
  s(j) = i;
  rowsToCheck(i) = false;
endfor
for i=find(rowsToCheck)
  j = find(C(i,colsToCheck) == 0);
  j = find(colsToCheck)(j(1));
  s(j) = i;
  colsToCheck(i) = false;
endfor

while(true)
  ## Step 11: Remove any pre-marks
  z = zeros(N,1);

  ## Step 3: check if N zeros are marked: then we are done
  if (length(find(s)) == N)
    optimumRows = s;
    ## restore broadcasting warning to previous state
    warning(save_state, "Octave:broadcast");
    return;
  endif

  ## Step 4: In the following we leave aside part of the columns
  ## (when proceeding manually one ticks these). Identify the
  ## 'unticked' columns that we are interested in.
  untickedCols = (s == 0);
  # following unnecessary: no rows ticked yet (z is zero)
  #untickedCols(!untickedCols) |= (z(s(!untickedCols)) > 0);

  ## Calculate row minima (of unticked elements)
  [ m, mi ] = min(cost(:, untickedCols) - u - v(untickedCols), [], 2);
  mi = find(untickedCols)(mi); ## convert to absolute indices

  ## Loop invariants and properties:
  ## * tickedRows elements can only transition from false to true
  while(true)
    tickedRows = (z > 0);

    ## Step 5
    [ h, hi ] = min(m(!tickedRows));
    iProtoMark = find(!tickedRows)(hi);
    jProtoMark = mi(iProtoMark);    

    ## Step 6: update node potentials
    if (h>0)
      v(untickedCols) += h;
      u(tickedRows) -= h;
      m -= h; # only valid for elements neither on a ticked row nor on
              # a ticked column, but thats all we need
    endif

    ## Step 7: pre-mark a zero among the unticked elements
    z(iProtoMark) = jProtoMark;

    ### Uncomment this section if you need debugging output
    #ticksColsRows = [ ! untickedCols', tickedRows' ]
    #tmp = zeros(N,N);
    #tmp(sub2ind([N, N], s(s>0), find(s>0))) = 1;
    #tmp2 = zeros(N,N);
    #tmp2(sub2ind([N, N], find(z>0), z(z>0))) = 1;
    #CostMarksPremarks = [ cost - u - v, tmp, tmp2 ]

    ## Step 8: If same row contains a marked zero, erase its tick, add
    ## row tick and iterate, otherwise exit loop
    jMarkSameRow = find(s == iProtoMark);
    if (length(jMarkSameRow) == 0)
      break;
    endif

    ## untick this column and add row tick
    untickedCols(jMarkSameRow) = true;
    tickedRows(iProtoMark) = true;

    ## need to update row minima
    [ m, mi2 ] = min([ m, cost(:, jMarkSameRow) - u - v(jMarkSameRow) ], [], 2);
    mi(mi2 == 2) = jMarkSameRow;
  endwhile

  do
    ## Step 9: Mark the found candidate
    ## Step 10: If the column contains another mark, remove mark, pick
    ## the pre-marked zero in its row and loop
    prevMarkRow = s(jProtoMark);
    s(jProtoMark) = iProtoMark;
    if (prevMarkRow > 0)
      iProtoMark = prevMarkRow;
      jProtoMark = z(prevMarkRow);
    endif
  until (prevMarkRow == 0);

end
