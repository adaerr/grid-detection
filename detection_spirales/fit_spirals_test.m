function phase_diff = fit_spirals_test()

load("-ascii","f2_phase/B-108_SB2020_000_f=0.7_phase.m")
phase_map=B_108_SB2020_000_f_0_7_phase;
N=42;
point=[1200 1030];
width=200;

params = fit_spirals_initial_estimate(phase_map, N, point, width)

w = columns(phase_map);
h = rows(phase_map);
x = ones(h,1) * ((1:w) - ceil(w/2));
y = ((1:h) - 0.9*ceil(h/2))' * ones(1,w);
rmax=0.95*w/2;
circmask = (x.*x + y.*y < rmax*rmax);

newparams = fit_spirals_bg(phase_map, N, params, circmask)

phase_diff = fit_spirals_calc_phase_diff(phase_map, N,
                                         newparams(1:2), newparams(3),
                                         circmask);

imshow(phase_diff, [-pi pi]);

endfunction

# 050 gouttelettes:
## N1=-60;
## N2=42;
## point1=[800 1200];
## w1=180;
## point2=[1600 1100];
## w2=180;
