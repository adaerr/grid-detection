function v=logspirals(x,y,xo,yo,nc,ncc)
  rx=x-xo;
  ry=yo-y;
  phi=atan2(ry,rx);
  b=log(sqrt(ry*ry+rx*rx));
  v=0.5+0.25*cos(ncc*(phi-b))+0.25*cos(nc*(phi+b))
endfunction
