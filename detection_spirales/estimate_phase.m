# Estimate the phase around the given point.
#
# phasemap ... phase measurement
# point,w ... center and width of a square on the phasemap that is
# reasonably free from large scale phase jumps. Typically (point) will
# point to the middle of an arbitrary phase stripe (in pixel coordinates,
# i.e. the top left element has coords 0,0), and w will be such that
# sqrt(2)*w is less than the local stripe width (in pixels)

function phase_diff = estimate_phase(phasemap,point,w)

offset = (1:w) - ceil(w/2);
# extract region of interest
point_row = point(2)+1;
point_column = point(1)+1;
region = phasemap(point_row + offset, point_column + offset);

phase_diff = median(reshape(region, 1, numel(region)));

endfunction
