## usage: lockin (name,burst,stride,f1,f2)

function [accumulator1,accumulator2] = lockin (name,burst,f1,f2)

  source(strcat(name,".meta"));
  stamps=dlmread(strcat(name,".stamps"));
  [rawfid,msg]=fopen(strcat(name,".raw"),"r");
  if ( rawfid < 0 )
    msg
    return;
  endif
  # subtract first row to have smaller values (only increment matters)
  stamps -= ones(rows(stamps),1)*stamps(1,:);

  imagesize=subRegionWidth*subRegionHeight;
  accumulator0 = zeros(subRegionWidth,subRegionHeight);
  accumulator1 = zeros(subRegionWidth,subRegionHeight);
  accumulator2 = zeros(subRegionWidth,subRegionHeight);

  for k=1:burst
    image = fread(rawfid,[subRegionWidth,subRegionHeight],"uint8");
    accumulator0 += image;
    accumulator1 += image*exp(2*pi*i*f1*1e-9*stamps(k,2));
    accumulator2 += image*exp(2*pi*i*f2*1e-9*stamps(k,2));
  endfor

  accumulator0 /= burst;
  correction1 = zeros(subRegionWidth,subRegionHeight);
  correction2 = zeros(subRegionWidth,subRegionHeight);

  for k=1:burst
    correction1 += accumulator0*exp(2*pi*i*f1*1e-9*stamps(k,2));
    correction2 += accumulator0*exp(2*pi*i*f2*1e-9*stamps(k,2));
  endfor

  accumulator1 -= correction1;
  accumulator2 -= correction2;

  fclose(rawfid);
endfunction
