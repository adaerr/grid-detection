# Estimate the starting parameters for the background fit of a spiral
# phase map.
#
# phase_map ... phase measurements
# point,width ... center and width of a square on the phasemap that is
# reasonably free from large scale phase jumps. Typically (point) will
# point to the middle of an arbitrary phase stripe (in pixel coordinates,
# i.e. the top left element has coords 0,0), and w will be such that
# sqrt(2)*w is less than the local stripe width (in pixels)
# N ... the number of spiral arms; positive values represent counterclockwise
# spirals, negative values stand for clockwise spirals
# center ... the estimated center of the spiral pattern, relative to the
# same origin as (point).

function params = fit_spirals_initial_estimate(phase_map, N, point, width)

center = estimate_spiral_center(phase_map, N, point, width);
phase_offset = estimate_phase(phase_map, point, width)
    - calc_phase(point(1), point(2), N, center);

params = [center; phase_offset];

endfunction
