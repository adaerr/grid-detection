# Return (height,width)-matrices x and y that contain the coordinate
# values corresponding to the given point with respect to the origin.
# Possibly corrects for the aberrations of the experimental set-up.

function [x y] = coordinate_matrices(width, height, center, C = 0)

###dX = (2*(0 : width-1) - width + 1) / width; # between -1 and +1
###xcorrected = (0 : width-1) + C*dX.^3;
###
###dY = (2*(0 : height-1) - height + 1) / width; # width used on purpose
###ycorrected = (0 : height-1) + C*dY.^3;
###
###x = xcorrected(ones(1,height),:) - center(1);
###y = ycorrected'(:,ones(1, width)) - center(2);

dX = (2*(0 : width-1) - width + 1) / width; # between -1 and +1
dY = (2*(0 : height-1) - height + 1) / width; # width used on purpose

x = dX(ones(1,height),:);
y = dY'(:,ones(1, width));

rsq = x.*x+y.*y;

xcorrected = (0 : width-1)(ones(1,height),:) + C*x.*rsq;
ycorrected = (0 : height-1)'(:,ones(1, width)) + C*y.*rsq;

x = xcorrected - center(1);
y = ycorrected - center(2);

endfunction
