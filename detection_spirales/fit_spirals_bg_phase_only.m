# minimise discrepancy between measurement and fit; same as
# fit_spirals_bg, but allow variations of phase offset (param(3)) only.
#
# This function should really be renamed to something like
# calc_mean_phasediff, as there is no fitting involved.

function newPhase = fit_spirals_bg_phase_only(phase_map, N, center, phaseOffset, mask, C=0)

phasediff = fit_spirals_calc_phase_diff(phase_map, N, center, 0, mask, C);
newPhase = arg(sum(sum(exp(1i*phasediff(mask)))));

endfunction
