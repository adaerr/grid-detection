# given a two frequency signal
fA=1;
fB=0.7;
phiA=0.25*pi;
phiB=0.65*pi;
T=10;
N=110;
Nsub=100;
t=T*(0:(N-1))/N;
s=cos(2*pi*fA*t+phiA)+cos(2*pi*fB*t+phiB);
figure(1)
plot(t,s)
# what are the Fourier components / scalar prod w monochromatic test functions
function p = scal(x,y) p = x*y'; endfunction
a=exp(2*pi*i*fA*t);
b=exp(2*pi*i*fB*t);
# check normalisation
scal(a,a)/N # should be 1
scal(b,b)/N # should be 1
# and check that we retrieve phase of the components of s
arg(scal(s,a)/N)/pi # should be 0.25
arg(scal(s,b)/N)/pi # should be 0.65
# now suppose only part of the signal could be retrieved (first Nsub samples)
nsub=1:Nsub;
tsub=t(nsub);
# but not the complement
ncomp=(Nsub+1):N;
tcomp=t(ncomp);
# extract sub-part and complement of signal and test functions
ssub=s(nsub);
asub=a(nsub);
bsub=b(nsub);
scomp=s(ncomp);
acomp=a(ncomp);
bcomp=b(ncomp);
# our test (sub)functions are no longer orthonormal
scal(asub,bsub)/N # non-zero
# and the extracted phases do not match the above values
phiA0=arg(scal(ssub,asub)/N);
phiB0=arg(scal(ssub,bsub)/N);
phiA0/pi # bit different from 0.25 (e.g. 0.25663)
phiB0/pi # bit different from 0.65 (e.g. 0.66068)
# now iterate, patching the signal in the missing region
kmax=100;
amplif=N/(N-Nsub);
phaseA = zeros(1,kmax);
phaseB = zeros(1,kmax);
for k = 1:kmax,
  sfakecomp=cos(2*pi*fA*tcomp+phiA0)+cos(2*pi*fB*tcomp+phiB0);
  phiA1=arg(scal(ssub,asub)+scal(sfakecomp,acomp));
  phiB1=arg(scal(ssub,bsub)+scal(sfakecomp,bcomp));
  phaseA(k)=phiA1/pi;
  phaseB(k)=phiB1/pi;
  phiA0=phiA1;#+amplif*(phiA1-phiA0);
  phiB0=phiB1;#+amplif*(phiB1-phiB0);
end
figure(2)
plot(phaseA-phiA/pi,'*')
hold on; plot(phaseB-phiB/pi,'r*'); hold off
# example (phiA=0.25*pi, phiB=0.65*pi, Nsub=800/N=1000):
# phiA 1: 0.25024
# phiA 2: 0.24990
# phiA 3: 0.24995
# phiA 4: 0.24998
# phiA 5: 0.25000
# phiB 1: 0.65146
# phiB 2: 0.65027
# phiB 3: 0.65006
# phiB 4: 0.65002
# phiB 5: 0.65001
