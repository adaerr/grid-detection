# minimise discrepancy between measurement and fit

function newparams = fit_spirals_bg(phase_map, N, center, phaseOffset, mask)

maxiter = 100;
params = [center phaseOffset];
o = optimset('MaxIter', maxiter, 'TolFun', 1E-10);
phi = @(params) fit_spirals_distance(phase_map, N,
                                     params(1:2), params(3), mask);
[x_optim, y_min, conv, iters, nevs] = powell(phi, params, o);

if (conv != 1 || iters >= maxiter)
  error("Background fit bailed out after %d iterations without converging.",
        iters);
endif

newparams = x_optim;

endfunction
