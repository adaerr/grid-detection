# minimise discrepancy between measurement and fit

function newparams = fit_spirals_bg_corrected(phase_map, N, center, phaseOffset, mask, C)

maxiter = 100;
par = [center phaseOffset C];
o = optimset('MaxIter', maxiter, 'TolFun', 1E-10);
phi = @(par) fit_spirals_distance(phase_map, N,
                                     par(1:2), par(3), mask, par(4));
[x_optim, y_min, conv, iters, nevs] = powell(phi, par, o);

if (conv != 1 || iters >= maxiter)
  error("Background fit bailed out after %d iterations without converging.",
        iters);
endif

newparams = x_optim;

endfunction
