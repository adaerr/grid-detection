# Return logarithmic spiral phase at point x,y given number of arms and center

function phi = calc_phase(x, y, N, center = [0 0])

x -= center(1);
y -= center(2);
phi = abs(N)*(atan2(-y, x) - 0.5*sign(N)*log(x.*x + y.*y));

endfunction
