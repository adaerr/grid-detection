/**************************************************************************
 * File: FindShortest.java
 *   Original File: Dijkstra.java by
 *   Author: Keith Schwarz (htiek@cs.stanford.edu)
 * Additional utility methods: Adrian Daerr (adrian@tortenboxer.de)
 *
 * An implementation of Dijkstra's single-source shortest path algorithm.
 * The algorithm takes as input a directed graph with non-negative edge
 * costs and a source node, then computes the shortest path from that node
 * to each other node in the graph.
 *
 * The algorithm works by maintaining a priority queue of nodes whose
 * priorities are the lengths of some path from the source node to the
 * node in question.  At each step, the algortihm dequeues a node from
 * this priority queue, records that node as being at the indicated
 * distance from the source, and then updates the priorities of all nodes
 * in the graph by considering all outgoing edges from the recently-
 * dequeued node to those nodes.
 *
 * In the course of this algorithm, the code makes up to |E| calls to
 * decrease-key on the heap (since in the worst case every edge from every
 * node will yield a shorter path to some node than before) and |V| calls
 * to dequeue-min (since each node is removed from the prioritiy queue
 * at most once).  Using a Fibonacci heap, this gives a very good runtime
 * guarantee of O(|E| + |V| lg |V|).
 *
 * This implementation relies on the existence of a FibonacciHeap class, also
 * from the Archive of Interesting Code.  You can find it online at
 *
 *         http://keithschwarz.com/interesting/code/?dir=fibonacci-heap
 */

import java.util.*; // For HashMap

public final class FindShortest {

    /**
     * Container class that serves as a return value of several of the
     * methods of the FindShortest class. Currently yields two maps:
     * one from nodes to predecessor nodes (in the direction of the
     * source), and one from nodes to distances from the source. By
     * construction, these maps contain only nodes visited by the
     * algorithm, which are in particular always reacheable from the
     * source. Both maps have the same size, and contain only nodes
     * for which the minimum distance and path is valid. Moreover, for
     * any node contained in these maps, all nodes on the path from
     * the source are also keys in the map, as are all nodes which are
     * closer to the source.
     */
    public static final class Results<Integer> {
        private Map<Integer, Integer> mPredecessor;
        private Map<Integer, Double> mDistance;

        /**
         * Returns map from nodes X to predecessor nodes P(X). On the
         * shortest path from the source node to a node X, the
         * predecessor node P(X) is the last node visited just prior
         * to reaching X. Recursively using this map on nodes will
         * eventually yield the source node, which maps to
         * <i>null</i>.
         */
        public Map<Integer, Integer> getPredecessor() {
            return Collections.unmodifiableMap(mPredecessor);
        }

        /**
         * Returns a map that for each node yields its minimum
         * distance to the source node. If the search radius was
         * limited in the invocation of the algorithm, all node
         * distances are at most equal to this radius. The source node
         * maps to zero (but is present only if the search radius was
         * not negative).
         */
        public Map<Integer, Double> getDistance() {
            return Collections.unmodifiableMap(mDistance);
        }
    }

    /**
     * Given a directed, weighted graph G, a source node s, and an
     * array of target nodes t[], builds a map of distances and
     * predecessors to all nodes until either the closest nTargets
     * target nodes have been reached or the distance to the source
     * node exceeds the value of radius.
     *
     * @param graph The graph upon which to run Dijkstra's algorithm.
     * @param source The source node in the graph.
     * @param target An array of target node(s) in the graph. A null
     * parameter means that all graph nodes are target nodes.
     * @param nTargets The number of target nodes that should be
     * reached. A negative or zero value means that all targets should
     * be reached.
     * @param radius The maximum distance that should be searched.
     * @return A structure containing information about the searched
     * sub-graph.
     * @see FindShortest.Results
     */
    public static Results<Integer>
        mapTargetsWithinRadius(DirectedGraphIntNodes graph, int source,
                               int[] target, int nTargets, double radius) {

        /* Create a Fibonacci heap storing the distances of unvisited nodes
         * from the source node.
         */
        FibonacciHeap<Integer> pq = new FibonacciHeap<Integer>();

        /* The Fibonacci heap uses an internal representation that hands back
         * Entry objects for every stored element.  This map associates each
         * node in the graph with its corresponding Entry.
         */
        Map<Integer, FibonacciHeap.Entry<Integer>> entries =
            new HashMap<Integer, FibonacciHeap.Entry<Integer>>();

        /* Maintain a map from nodes to their distances.  Whenever we expand a
         * node for the first time, we'll put it in here.
         */
        Map<Integer, Double> distance = new HashMap<Integer, Double>();

        /* Also maintain a map from nodes to their immediate
         * predecessor on the path connecting to the source. Whenever
         * we find a new shortest path to a given node, we'll put it
         * in here.
         */
        Map<Integer, Integer> predecessor = new HashMap<Integer, Integer>();
        predecessor.put(source, null);

        /* If no target nodes are specified, suppose that all nodes
         * are targets. If nTargets is zero or negative, suppose that
         * all targets should be reached. In any case initialize
         * counter.
         */
        Set<Integer> tSet = new TreeSet<Integer>();
        if (target == null) {
            for (Integer t: graph)
                tSet.add(t);
        } else {
            for (int t: target)
                tSet.add(t);
        }
        if (nTargets <= 0)
            nTargets = tSet.size();
        int targetsReached = 0;

        /* Add each node to the Fibonacci heap at distance +infinity since
         * initially all nodes are unreachable.
         */
        for (Integer node: graph) 
            entries.put(node, pq.enqueue(node, Double.POSITIVE_INFINITY));

        /* Update the source so that it's at distance 0.0 from itself; after
         * all, we can get there with a path of length zero!
         */
        pq.decreaseKey(entries.get(source), 0.0);

        /* Keep processing the queue until no nodes remain. */
        while (!pq.isEmpty()) {
            /* Grab the current node.  The algorithm guarantees that we now
             * have the shortest distance to it.
             */
            FibonacciHeap.Entry<Integer> curr = pq.dequeueMin();

            /* Abort algorithm if maximum distance has been exceeded */
            if (curr.getPriority() > radius) break;

            /* Store this in the distance table. */
            distance.put(curr.getValue(), curr.getPriority());

            /* Check if this node was one of the target nodes, and keep count */
            if (tSet.contains(curr.getValue())) {
                targetsReached++;
                if (targetsReached >= nTargets) break;
            }

            /* Update the priorities of all of its edges. */
            for (Map.Entry<Integer, Double> arc : graph.edgesFrom(curr.getValue()).entrySet()) {
                /* If we already know the shortest path from the source to
                 * this node, don't add the edge.
                 */
                if (distance.containsKey(arc.getKey())) continue;

                /* Compute the cost of the path from the source to this node,
                 * which is the cost of this node plus the cost of this edge.
                 */
                double pathCost = curr.getPriority() + arc.getValue();

                /* If the length of the best-known path from the source to
                 * this node is longer than this potential path cost, update
                 * the cost of the shortest path.
                 */
                FibonacciHeap.Entry<Integer> dest = entries.get(arc.getKey());
                if (pathCost < dest.getPriority()) {
                    pq.decreaseKey(dest, pathCost);
                    predecessor.put(dest.getValue(), curr.getValue());
                }
            }
        }

        /* Some nodes might have predecessor nodes but no distance to
         * the source. Those nodes are endpoints of tentative paths
         * which are not confirmed to be the shortest paths (the
         * algorithm bailed out before popping the corresponding node
         * from the priority queue, or it was the last popped node and
         * radius was exceeded). Identify and delete those nodes so
         * that the result maps contain only nodes for which the
         * minimum distance and path properties are valid.
         */
        Set<Integer> borderNodes = new HashSet<Integer>(predecessor.keySet());
        for (Integer n: distance.keySet()) {
            borderNodes.remove(n);
        }
        // Now borderNodes contains only nodes which have a
        // 'predecessor' but no (minimal) 'distance' value: those
        // could possibly be reached through another node at a shorter
        // distance, so remove them.
        for (Integer n: borderNodes) {
            predecessor.remove(n);
        }

        /* Package the distances and predecessor maps into a structure
         * we can return.
         */
        Results<Integer> result = new Results<Integer>();
        result.mDistance = distance;
        result.mPredecessor = predecessor;

        /* Finally, report the distances and predecessors we've found. */
        return result;
    }

    /**
     * Given a directed, weighted graph G, a source node s, and an
     * array of target nodes t[], builds a map of distances and
     * predecessors to all nodes until the closest nTargets target
     * nodes have been reached.
     * 
     * @return mapTargetsWithinRadius(..., radius =
     * Double.POSITIVE_INFINITY)
     * @see #mapTargetsWithinRadius
     */
    public static Results<Integer>
        mapTargets(DirectedGraphIntNodes graph, int source,
                   int[] target, int nTargets) {
        return mapTargetsWithinRadius(graph, source, target, nTargets,
                                      Double.POSITIVE_INFINITY);
    }

    /**
     * Given a directed, weighted graph G and a source node s, builds
     * a map of distances and predecessors to all nodes that are at a
     * distance from the source not exceeding radius.
     * 
     * @return mapTargetsWithinRadius(..., target = null,
     * nTargets = 0, radius)
     * @see #mapTargetsWithinRadius
     */
    public static Results<Integer>
        mapWithinRadius(DirectedGraphIntNodes graph, int source, double radius) {
        return mapTargetsWithinRadius(graph, source, null, 0, radius);
    }

    /**
     * Given a directed, weighted graph G and a source node s, builds
     * a map of distances and predecessors to all nodes.
     * 
     * @return mapTargetsWithinRadius(..., target = null,
     * nTargets = 0, radius = Double.POSITIVE_INFINITY)
     * @see #mapTargetsWithinRadius
     */
    public static Results<Integer>
        map(DirectedGraphIntNodes graph, int source) {
        return mapTargetsWithinRadius(graph, source, null, 0,
                                      Double.POSITIVE_INFINITY);
    }

    /**
     * Given a directed, weighted graph G, a source node s, and an
     * array of target nodes t[], builds a map of predecessors to all
     * nodes until either the closest nTargets target nodes have been
     * reached or the distance to the source node exceeds the value of
     * radius.
     *
     * @return mapTargetsWithinRadius(...).getPredecessor()
     * @see #mapTargetsWithinRadius
     * @see FindShortest.Results#getPredecessor
     */
    public static Map<Integer, Integer> 
        predecessorTargetsWithinRadius(DirectedGraphIntNodes graph,
                                       int source, int[] target,
                                       int nTargets, double radius) {
        return mapTargetsWithinRadius(graph, source, target, nTargets,
                                      radius).getPredecessor();
    }

    /**
     * Given a directed, weighted graph G, a source node s, and an
     * array of target nodes t[], builds a map of predecessors to all
     * nodes until the closest nTargets target nodes have been
     * reached.
     * 
     * @return mapTargetsWithinRadius(..., radius =
     * Double.POSITIVE_INFINITY)
     * @see #mapTargets
     * @see FindShortest.Results#getPredecessor
     */
    public static Map<Integer, Integer>
        predecessorTargets(DirectedGraphIntNodes graph, int source,
                           int[] target, int nTargets) {
        return mapTargets(graph, source, target,
                          nTargets).getPredecessor();
    }

    /**
     * Given a directed, weighted graph G and a source node s, builds
     * a map of predecessors to all nodes at most at a distance radius
     * from the source.
     *
     * @return mapWithinRadius(...).getPredecessor()
     * @see #mapWithinRadius
     * @see FindShortest.Results#getPredecessor
     */
    public static Map<Integer, Integer>
        predecessorWithinRadius(DirectedGraphIntNodes graph, int source,
                                double radius) {
        return mapWithinRadius(graph, source, radius).getPredecessor();
    }

    /**
     * Given a directed, weighted graph G and a source node s, builds
     * a map of predecessors to all nodes.
     * 
     * @return map(...).getPredecessor()
     * @see #map
     * @see FindShortest.Results#getPredecessor
     */
    public static Map<Integer, Integer>
        predecessor(DirectedGraphIntNodes graph, int source) {
        return map(graph, source).getPredecessor();
    }


    /**
     * Given a directed, weighted graph G, a source node s, and an
     * array of target nodes t[], builds a map of distances to all
     * nodes until either the closest nTargets target nodes have been
     * reached or the distance to the source node exceeds the value of
     * radius. Should be equivalent to
     * mapTargetsWithinRadius(...).getDistance() but uses its
     * own implementation to spare the memory burden of tracking
     * predecessor nodes.
     *
     * @return A map indicating the minimal distance of visited nodes
     * to the source node
     * @see #mapTargetsWithinRadius
     * @see FindShortest.Results#getDistance
     */
    public static Map<Integer, Double> 
        distanceTargetsWithinRadius(DirectedGraphIntNodes graph,
                                    int source, int[] target,
                                    int nTargets, double radius) {

        /* Create a Fibonacci heap storing the distances of unvisited nodes
         * from the source node.
         */
        FibonacciHeap<Integer> pq = new FibonacciHeap<Integer>();

        /* The Fibonacci heap uses an internal representation that hands back
         * Entry objects for every stored element.  This map associates each
         * node in the graph with its corresponding Entry.
         */
        Map<Integer, FibonacciHeap.Entry<Integer>> entries =
            new HashMap<Integer, FibonacciHeap.Entry<Integer>>();

        /* Maintain a map from nodes to their distances.  Whenever we expand a
         * node for the first time, we'll put it in here.
         */
        Map<Integer, Double> distance = new HashMap<Integer, Double>();

        /* If no target nodes are specified, suppose that all nodes
         * are targets. If nTargets is zero or negative, suppose that
         * all targets should be reached. In any case initialize
         * counter.
         */
        Set<Integer> tSet = new TreeSet<Integer>();
        if (target == null) {
            for (Integer t: graph)
                tSet.add(t);
        } else {
            for (int t: target)
                tSet.add(t);
        }
        if (nTargets <= 0)
            nTargets = tSet.size();
        int targetsReached = 0;

        /* Add each node to the Fibonacci heap at distance +infinity since
         * initially all nodes are unreachable.
         */
        for (Integer node: graph) 
            entries.put(node, pq.enqueue(node, Double.POSITIVE_INFINITY));

        /* Update the source so that it's at distance 0.0 from itself; after
         * all, we can get there with a path of length zero!
         */
        pq.decreaseKey(entries.get(source), 0.0);

        /* Keep processing the queue until no nodes remain. */
        while (!pq.isEmpty()) {
            /* Grab the current node.  The algorithm guarantees that we now
             * have the shortest distance to it.
             */
            FibonacciHeap.Entry<Integer> curr = pq.dequeueMin();

            /* Abort algorithm if maximum distance has been exceeded */
            if (curr.getPriority() > radius) break;

            /* Store this in the distance table. */
            distance.put(curr.getValue(), curr.getPriority());

            /* Check if this node was one of the target nodes, and keep count */
            if (tSet.contains(curr.getValue())) {
                targetsReached++;
                if (targetsReached >= nTargets) break;
            }

            /* Update the priorities of all of its edges. */
            for (Map.Entry<Integer, Double> arc : graph.edgesFrom(curr.getValue()).entrySet()) {
                /* If we already know the shortest path from the source to
                 * this node, don't add the edge.
                 */
                if (distance.containsKey(arc.getKey())) continue;

                /* Compute the cost of the path from the source to this node,
                 * which is the cost of this node plus the cost of this edge.
                 */
                double pathCost = curr.getPriority() + arc.getValue();

                /* If the length of the best-known path from the source to
                 * this node is longer than this potential path cost, update
                 * the cost of the shortest path.
                 */
                FibonacciHeap.Entry<Integer> dest = entries.get(arc.getKey());
                if (pathCost < dest.getPriority())
                    pq.decreaseKey(dest, pathCost);
            }
        }

        /* Finally, report the distances we've found. */
        return distance;
    }

    /**
     * Given a directed, weighted graph G, a source node s, and an
     * array of target nodes t[], builds a map of distances to all
     * nodes until the closest nTargets target nodes have been
     * reached.
     * 
     * @return A map indicating the minimal distance of visited nodes
     * to the source node,<br /> =
     * distanceTargetsWithinRadius(..., radius =
     * Double.POSITIVE_INFINITY)
     * @see #distanceTargetsWithinRadius
     * @see FindShortest.Results#getDistance
     */
    public static Map<Integer, Double>
        distanceTargets(DirectedGraphIntNodes graph, int source,
                        int[] target, int nTargets) {
        return distanceTargetsWithinRadius(graph, source, target,
                                           nTargets, Double.POSITIVE_INFINITY);
    }

    /**
     * Given a directed, weighted graph G and a source node s, builds
     * a map of distances to all nodes at most at a distance radius
     * from the source.
     *
     * @return A map indicating the minimal distance of visited nodes
     * to the source node,<br /> = distanceTargetsWithinRadius(graph,
     * source, null, 0, radius)
     * @see #distanceTargetsWithinRadius
     * @see FindShortest.Results#getDistance
     */
    public static Map<Integer, Double>
        distanceWithinRadius(DirectedGraphIntNodes graph, int source,
                             double radius) {
        return distanceTargetsWithinRadius(graph, source, null, 0, radius);
    }

    /**
     * Given a directed, weighted graph G and a source node s, produces the
     * distances from s to each other node in the graph.  If any nodes in
     * the graph are unreachable from s, they will be reported at distance
     * +infinity.
     *
     * @param graph The graph upon which to run Dijkstra's algorithm.
     * @param source The source node in the graph.
     * @return A map from nodes in the graph to their distances from
     * the source.<br /> = distanceWithinRadius(..., radius =
     * Double.POSITIVE_INFINITY)
     * @see #map
     * @see FindShortest.Results#getDistance
     */
    public static Map<Integer, Double>
        distance(DirectedGraphIntNodes graph, int source) {
        return distanceWithinRadius(graph, source, Double.POSITIVE_INFINITY);
    }

    /**
     * Given a directed, weighted graph G and a source node s,
     * produces the distances from s to each other node in the graph.
     * Same as distance() method.
     *
     * @see #distance
     */
    public static Map<Integer, Double>
        shortestPaths(DirectedGraphIntNodes graph, int source) {
        return distance(graph, source);
    }

    /**
     * Given a directed, weighted graph G, a source node s and a
     * target node t[], finds the shortest path from s to t.
     *
     * @param graph The graph upon which to run Dijkstra's algorithm.
     * @param source The source node in the graph.
     * @param target The target node in the graph.
     * @return An array of all nodes lying on the path
     * from source to target (including endpoints)
     */
    public static int[] //<Integer> List<Integer>
        path(DirectedGraphIntNodes graph, int source, int target) {
        final int[] t = new int[1];
        t[0] = target;
        Map<Integer, Integer> pred =
            predecessorTargets(graph, source, t, 1);
        ArrayList<Integer> p = new ArrayList<Integer>();
        Integer node = target;
        do {
            p.add(node);
            node = pred.get(node);
        } while (node != null);
        Collections.reverse(p);
        // Convert to primitive array (via Stream<Integer> -> IntStream)
        return p.stream().mapToInt(Integer::valueOf).toArray
    }

    /**
     * Simple utility method that returns a distance array
     * corresponding to a given array of nodes. The distance map is
     * typically that obtained through one of the other methods of
     * this class. If the map does not contain one of the given nodes,
     * the corresponding distance is set to positive infinity.
     */
    public static double[] 
        distances(Map<Integer, Double> distanceMap, int[] targets) {
        double[] d = new double[targets.length];
        for (int i=0; i<targets.length; i++) {
            if (distanceMap.containsKey(targets[i]))
                d[i] = distanceMap.get(targets[i]);
            else
                d[i] = Double.POSITIVE_INFINITY;
        }
        return d;
    }
}
